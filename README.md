
//...................................................................
//...SSSSSSS.........................................ttt.......iiii..
//..SSSSSSSSS.......................................tttt.......iiii..
//..SSSSSSSSSS......................................tttt.............
//.SSSSS..SSSS........aaaaaa........nnnnnnnn...... tttttt.....iiii..
//.SSSSS.............aaaaaaaa.......nnnnnnnnn..... tttttt.....iiii..
//..SSSSSSS......... aaa.aaaaa......nnnn.nnnnn......tttt.......iiii..
//...SSSSSSSSS..........aaaaaa......nnnn..nnnn......tttt.......iiii..
//.....SSSSSSS.......aaaaaaaaa......nnnn..nnnn......tttt.......iiii..
//........SSSSS..... aaaaaaaaa......nnnn..nnnn......tttt.......iiii..
//.SSSS....SSSS..... aaa.aaaaa......nnnn..nnnn......tttt.......iiii..
//.SSSSSSSSSSSS..... aaa.aaaaa......nnnn..nnnn......tttt.......iiii..
//..SSSSSSSSSS...... aaaaaaaaa......nnnn..nnnn......tttttt.....iiii..
//...SSSSSSSS........aaaaaaaaa......nnnn..nnnn......tttttt.....iiii..
//...................................................................


Welcome to your Node.js project on Cloud9 IDE!

This chat example showcases how to use `socket.io` with a static `express` server.

## Running the server

1) Open `server.js` and start the app by clicking on the "Run" button in the top menu.

2) Alternatively you can launch the app from the Terminal:

    $ node server.js

Once the server is running, open the project in the shape of 'https://projectname-username.c9users.io/'. As you enter your name, watch the Users list (on the left) update. Once you press Enter or Send, the message is shared with all connected clients.
